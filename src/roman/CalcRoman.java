package roman;

import interfaces.Calculatable;
import utils.Transfer;

import static utils.CalcOperationResult.getResult;

public class CalcRoman implements Calculatable {

    @Override
    public String calc(String input) throws Exception {

        int len = input.split(" ").length;
        if (len != 3) throw new Exception("Something wrong!");

        String a = input.split(" ")[0];
        String operation = input.split(" ")[1];
        String b = input.split(" ")[2];
        String stringResult = "";

        int arabianA, arabianB, result;

        try {

            arabianA = Transfer.romanToArabic(a);
            arabianB = Transfer.romanToArabic(b);

            result = getResult(operation, arabianA, arabianB);

            if (result < 1) throw new Exception();

            stringResult = Transfer.arabicToRoman(result);

        } catch (Exception e) {
            e.printStackTrace();
        }
        return stringResult;
    }
}
