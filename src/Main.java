import arabian.CalcArabian;
import roman.CalcRoman;

import java.util.Scanner;

public class Main {
    static CalcRoman calcRoman = new CalcRoman();
    static CalcArabian calcArabian = new CalcArabian();

    public static void main(String[] args) throws Exception {
        String input = new Scanner(System.in).nextLine();
        System.out.println(calc(input));
    }

    public static String calc(String input) throws Exception {
        String firstNum = input.split(" ")[0], secondNum = input.split(" ")[2], result;

        try {
             Integer.parseInt(firstNum);
             Integer.parseInt(secondNum);

            result = calcArabian.calc(input);

        } catch (Exception e) {
            result = calcRoman.calc(input);
        }
        return result;
    }

}
