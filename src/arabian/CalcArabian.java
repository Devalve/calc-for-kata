package arabian;

import interfaces.Calculatable;

import static utils.CalcOperationResult.getResult;

public class CalcArabian implements Calculatable {

    @Override
    public String calc(String input) throws Exception {

        int len = input.split(" ").length;
        if (len != 3) throw new Exception("Something wrong!");


        String a = input.split(" ")[0];
        String operation = input.split(" ")[1];
        String b = input.split(" ")[2];


        int result = 0, parsedA = Integer.parseInt(a), parsedB = Integer.parseInt(b);

        try {
            result = getResult(operation, parsedA, parsedB);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return String.valueOf(result);
    }


}
