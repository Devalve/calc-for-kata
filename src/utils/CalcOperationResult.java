package utils;

public class CalcOperationResult {
    public static int getResult(String operation, int a, int b) throws Exception {
        int result;
        switch (operation) {
            case "+" -> result = a + b;
            case "-" -> result = a - b;
            case "/" -> result = a / b;
            case "*" -> result = a * b;
            default -> throw new Exception();
        }
        return result;
    }
}
