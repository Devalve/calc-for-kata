package interfaces;

public interface Calculatable {

      String calc(String input) throws Exception;
}
